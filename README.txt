- Propriedades Mysql:

Host: 127.0.0.1
Port: 3306
User: root
Password: root
DB: chrisproject
Table: pessoa

- Propriedades Postgresql

Host: 127.0.0.1
Port: 5432
User: postgres
Password: postgres
DB: chrisproject
Table: pessoa

- PG_DUMP

$ pg_dump.exe -Fc -Z 9 --username "postgres" -f C:\Users\Vandhuy\postgresql.dump chrisproject

- PG_RESTORE

$ pg_restore.exe -F c --username "postgres" -d chrisproject C:\Users\Vandhuy\postgresql.dump