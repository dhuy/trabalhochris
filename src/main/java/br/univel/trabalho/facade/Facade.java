package br.univel.trabalho.facade;

import br.univel.trabalho.worker.*;

/**
 * @author Christopher
 */
public class Facade {
    public void search(String textToSearch, String filePath) throws InterruptedException {
        new Thread(new MysqlWorker(textToSearch), "PsqlThread").start();
        new Thread(new PsqlWorker(textToSearch), "MysqlThread").start();
        new Thread(new FileWorker(textToSearch, filePath), "FileThread").start();
        new Thread(new GoogleWorker(textToSearch), "GoogleThread").start();
    }
}
