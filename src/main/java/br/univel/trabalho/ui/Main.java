package br.univel.trabalho.ui;

import br.univel.trabalho.facade.Facade;
import br.univel.trabalho.util.UIComponents;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * @author Christopher
 */
public class Main extends javax.swing.JFrame {
    private JFrame jframe = null;
    private Facade facade = null;
    
    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        keepUIComponents();
        
        this.jframe = this;
        this.setLocationRelativeTo(null);
        this.setTitle("Buscador de tudo");
        
        facade = new Facade();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jTabbedPanel = new javax.swing.JTabbedPane();
        psqlPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        psqlTable = new javax.swing.JTable();
        mysqlPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        mysqlTable = new javax.swing.JTable();
        filePanel = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        fileTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jPathField = new javax.swing.JTextField();
        jSelectDirectoryBtn = new javax.swing.JButton();
        googlePanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        googleTable = new javax.swing.JTable();
        searchField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        psqlTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IdPessoa", "Nome", "Idade", "Profissão"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        psqlTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(psqlTable);
        if (psqlTable.getColumnModel().getColumnCount() > 0) {
            psqlTable.getColumnModel().getColumn(0).setResizable(false);
            psqlTable.getColumnModel().getColumn(1).setResizable(false);
            psqlTable.getColumnModel().getColumn(2).setResizable(false);
            psqlTable.getColumnModel().getColumn(3).setResizable(false);
        }

        javax.swing.GroupLayout psqlPanel1Layout = new javax.swing.GroupLayout(psqlPanel1);
        psqlPanel1.setLayout(psqlPanel1Layout);
        psqlPanel1Layout.setHorizontalGroup(
            psqlPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 723, Short.MAX_VALUE)
        );
        psqlPanel1Layout.setVerticalGroup(
            psqlPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );

        jTabbedPanel.addTab("Postgresql", psqlPanel1);

        mysqlTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IdPessoa", "Nome", "Idade", "Profissão"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        mysqlTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(mysqlTable);
        if (mysqlTable.getColumnModel().getColumnCount() > 0) {
            mysqlTable.getColumnModel().getColumn(0).setResizable(false);
            mysqlTable.getColumnModel().getColumn(1).setResizable(false);
            mysqlTable.getColumnModel().getColumn(2).setResizable(false);
            mysqlTable.getColumnModel().getColumn(3).setResizable(false);
        }

        javax.swing.GroupLayout mysqlPanelLayout = new javax.swing.GroupLayout(mysqlPanel);
        mysqlPanel.setLayout(mysqlPanelLayout);
        mysqlPanelLayout.setHorizontalGroup(
            mysqlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 723, Short.MAX_VALUE)
        );
        mysqlPanelLayout.setVerticalGroup(
            mysqlPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );

        jTabbedPanel.addTab("Mysql", mysqlPanel);

        fileTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Resultados"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        fileTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(fileTable);
        if (fileTable.getColumnModel().getColumnCount() > 0) {
            fileTable.getColumnModel().getColumn(0).setResizable(false);
        }

        jLabel1.setText("Path:");

        jPathField.setBackground(new java.awt.Color(240, 240, 240));
        jPathField.setText(".\\\\texts");
        jPathField.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jPathField.setFocusable(false);

        jSelectDirectoryBtn.setText("Selecionar Path");
        jSelectDirectoryBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jSelectDirectoryBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout filePanelLayout = new javax.swing.GroupLayout(filePanel);
        filePanel.setLayout(filePanelLayout);
        filePanelLayout.setHorizontalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 723, Short.MAX_VALUE)
            .addGroup(filePanelLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPathField)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSelectDirectoryBtn))
        );
        filePanelLayout.setVerticalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, filePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(jPathField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSelectDirectoryBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPanel.addTab("Arquivo", filePanel);

        googleTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Links encontrados pela API Google"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        googleTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(googleTable);
        if (googleTable.getColumnModel().getColumnCount() > 0) {
            googleTable.getColumnModel().getColumn(0).setResizable(false);
        }

        javax.swing.GroupLayout googlePanelLayout = new javax.swing.GroupLayout(googlePanel);
        googlePanel.setLayout(googlePanelLayout);
        googlePanelLayout.setHorizontalGroup(
            googlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 723, Short.MAX_VALUE)
        );
        googlePanelLayout.setVerticalGroup(
            googlePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(googlePanelLayout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPanel.addTab("Google", googlePanel);

        searchField.setText("Critério de busca...");
        searchField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                searchFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                searchFieldFocusLost(evt);
            }
        });
        searchField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                searchFieldonEnter(evt);
            }
        });

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPanel)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, 727, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 446, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 750, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void keepUIComponents() {
        UIComponents.keepMysqlTable(mysqlTable);
        UIComponents.keepPsqlTable(psqlTable);
        UIComponents.keepFileTable(fileTable);
        UIComponents.keepGoogleTable(googleTable);
    }
    
    private void searchFieldonEnter(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchFieldonEnter
        if (evt.getKeyCode() == 10) {
            if (searchField.getText().equals(""))
                JOptionPane.showMessageDialog(null, "Por favor, digite algo...", "Atenção", JOptionPane.WARNING_MESSAGE);
            
            try {
                facade.search(searchField.getText(), jPathField.getText());
            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }//GEN-LAST:event_searchFieldonEnter

    private void searchFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_searchFieldFocusGained
        searchField.setText("");
    }//GEN-LAST:event_searchFieldFocusGained

    private void searchFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_searchFieldFocusLost
        searchField.setText("Critério de busca...");
    }//GEN-LAST:event_searchFieldFocusLost

    private void jSelectDirectoryBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jSelectDirectoryBtnMouseClicked
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        if (jfc.showSaveDialog(this) == 1){
            jPathField.setText("");
        } else {
            File file = jfc.getSelectedFile();
            jPathField.setText(file.getPath());
        }
    }//GEN-LAST:event_jSelectDirectoryBtnMouseClicked
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel filePanel;
    private javax.swing.JTable fileTable;
    private javax.swing.JPanel googlePanel;
    private javax.swing.JTable googleTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField jPathField;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JButton jSelectDirectoryBtn;
    private javax.swing.JTabbedPane jTabbedPanel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel mysqlPanel;
    private javax.swing.JTable mysqlTable;
    private javax.swing.JPanel psqlPanel1;
    private javax.swing.JTable psqlTable;
    private javax.swing.JTextField searchField;
    // End of variables declaration//GEN-END:variables
}
