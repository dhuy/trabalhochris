package br.univel.trabalho.worker;

import br.univel.trabalho.util.UIComponents;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * @author Christopher
 */
public class FileReaderWorker implements Runnable {
    private String textToSearch;
    private String path;
    
    public FileReaderWorker(String textToSearch, String path) {
        this.textToSearch = textToSearch;
        this.path = path;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " executing a FileReaderWorker.");
        
        try {
            File file = new File(this.path);
            String t_text = new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);
            if (t_text.contains(this.textToSearch)) {
                UIComponents.createFileTableRow("Encontrado correspondência em " + this.path);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
