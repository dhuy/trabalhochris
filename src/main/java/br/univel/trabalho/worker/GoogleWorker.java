package br.univel.trabalho.worker;

import br.univel.trabalho.util.GoogleSearchAPI;
import br.univel.trabalho.util.UIComponents;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Christopher
 */
public class GoogleWorker implements Runnable {
    private String textToSearch;
    
    public GoogleWorker(String textToSearch) {
        this.textToSearch = textToSearch;
    }
    
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " executing a GoogleWorker.");
        
        UIComponents.clearJTable("googleTable");
        
        GoogleSearchAPI api = new GoogleSearchAPI(this.textToSearch);
        
        try {
            Collection<String> list = api.call();
            Iterator<String> iterator = list.iterator();
            
            while (iterator.hasNext()) {
                UIComponents.createGoogleTableRow(iterator.next());
            }
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
