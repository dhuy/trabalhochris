package br.univel.trabalho.worker;

import br.univel.trabalho.util.UIComponents;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * @author Christopher
 */
public class PsqlWorker implements Runnable {
    private Properties properties;
    private String connectionUrl, mainQuery, textToSearch;
        
    public PsqlWorker(String textToSearch) {
        this.properties = new Properties();
        this.textToSearch = textToSearch;
        
        setProperties();
    }
    
    private void setProperties() {
        this.properties.setProperty("user", "postgres");
        this.properties.setProperty("password", "postgres");
        this.properties.setProperty("useSSL", "false");
        
        this.connectionUrl = "jdbc:postgresql://127.0.0.1:5432/chrisproject";

        if (UIComponents.isInteger(this.textToSearch)) {
            this.mainQuery = "SELECT * FROM pessoa WHERE id='" + this.textToSearch + "' OR nome LIKE '%" + this.textToSearch + "%' "
                + "OR idade LIKE '%" + this.textToSearch + "%' OR profissao LIKE '%" + this.textToSearch + "%'";
        } else {
            this.mainQuery = "SELECT * FROM pessoa WHERE nome LIKE '%" + this.textToSearch + "%' OR idade LIKE '%" + this.textToSearch + "%' OR profissao LIKE '%" + this.textToSearch + "%'";
        }
    }
        
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " executing a PsqlWorker.");
        
        UIComponents.clearJTable("psqlTable");
        
        try {
            Connection conn = DriverManager.getConnection(this.connectionUrl, this.properties);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(this.mainQuery);

            while (rs.next())
            {
                UIComponents.createPsqlTableRow(rs.getString("id"), rs.getString("nome"),
                        rs.getString("idade"), rs.getString("profissao"));
            }
            
            rs.close(); st.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
