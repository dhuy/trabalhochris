package br.univel.trabalho.worker;

import br.univel.trabalho.util.UIComponents;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Christopher
 */
public class FileWorker implements Runnable {
    private String textToSearch;
    private String filePath;
    
    public FileWorker(String textToSearch, String filePath) {
        this.textToSearch = textToSearch;
        this.filePath = filePath;
    }
    
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " executing a FileWorker.");
        
        UIComponents.clearJTable("fileTable");
            
        List<File> files = (List<File>) FileUtils.listFiles(new File(this.filePath), new String[] { "txt" }, true);
        for (int i = 0; i < files.size(); i++) {
            try {
                new Thread(new FileReaderWorker(this.textToSearch, files.get(i).getCanonicalPath()), "FileReaderThread_" + (i + 1)).start();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
