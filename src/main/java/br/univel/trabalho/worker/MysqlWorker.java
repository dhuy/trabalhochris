package br.univel.trabalho.worker;

import br.univel.trabalho.util.UIComponents;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * @author Christopher
 */
public class MysqlWorker implements Runnable {
    private Properties properties;
    private String connectionUrl, mainQuery, textToSearch;
    
    public MysqlWorker(String textToSearch) {
        this.properties = new Properties();
        this.textToSearch = textToSearch;
        
        setProperties();
    }
    
    private void setProperties() {
        this.properties.setProperty("user", "root");
        this.properties.setProperty("password", "root");
        this.properties.setProperty("useSSL", "false");
        
        this.connectionUrl = "jdbc:mysql://127.0.0.1:3306/chrisproject";
        
        this.mainQuery = "SELECT * FROM pessoa WHERE id LIKE '%" + this.textToSearch + "%' OR nome LIKE '%" + this.textToSearch + "%' OR "
                + "idade LIKE '%" + this.textToSearch + "%' OR profissao LIKE '%" + this.textToSearch + "%'";
    }
    
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " executing a MysqlWorker.");
        
        UIComponents.clearJTable("mysqlTable");
        
        try {
            Connection conn = DriverManager.getConnection(this.connectionUrl, this.properties);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(this.mainQuery);

            while (rs.next())
            {
              UIComponents.createMysqlTableRow(rs.getString("id"), rs.getString("nome"),
                      rs.getString("idade"), rs.getString("profissao"));
            }

            rs.close(); st.close();
        }
        catch (SQLException e)
        {
          System.err.println(e.getMessage());
        }
    }
}
