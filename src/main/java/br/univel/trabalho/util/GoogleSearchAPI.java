package br.univel.trabalho.util;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GoogleSearchAPI implements Callable<Collection<String>> {
    private static final String GOOGLE_SEARCH_URL = "https://www.google.com/search?q=%s&num=%d";
    private static final Integer numberOfRegister = 15;
    private final String query;

    public GoogleSearchAPI(final String query) {
        this.query = query;
    }

    @Override
    public Collection<String> call() throws IOException {
        final String searchURL = String.format(GOOGLE_SEARCH_URL, query, numberOfRegister);
        final Document doc = Jsoup.connect(searchURL).userAgent("Mozilla/5.0").get();
        final Elements results = doc.select("h3.r > a");
        final Collection<String> response = new ArrayList<String>(numberOfRegister);
        
        for (final Element result : results) {
            final String linkHref = result.attr("href");
            final String realLink = linkHref.substring(7, linkHref.indexOf("&"));
            response.add(realLink);
        }

        return Collections.unmodifiableCollection(response);
    }
}
