package br.univel.trabalho.util;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * @author Christopher
 */
public class UIComponents {
    private static UIComponents instance;
    private static JTable mysqlTable;
    private static JTable psqlTable;
    private static JTable fileTable;
    private static JTable googleTable;
    
    private UIComponents() {
    }
    
    public static UIComponents getInstance() {
        if (instance == null)
            instance = new UIComponents();
        
        return instance;
    }
    
    /*
    * Keep Methods
    */
    public static void keepMysqlTable(JTable mysqlTableParam) {
        mysqlTable = mysqlTableParam;
    }
    
    public static void keepPsqlTable(JTable psqlTableParam) {
        psqlTable = psqlTableParam;
    }
    
    public static void keepFileTable(JTable fileTableParam) {
        fileTable = fileTableParam;
    }
    
    public static void keepGoogleTable(JTable googleTableParam) {
        googleTable = googleTableParam;
    }
    
    /*
    * Clear Method
    */
    public static void clearJTable(String type) {
        DefaultTableModel model = null;
        
        switch (type) {
            case "psqlTable":
                model = (DefaultTableModel) psqlTable.getModel();
                break;
                
            case "mysqlTable":
                model = (DefaultTableModel) mysqlTable.getModel();
                break;
                
            case "fileTable":
                model = (DefaultTableModel) fileTable.getModel();
                break;
                
            case "googleTable":
                model = (DefaultTableModel) googleTable.getModel();
                break;
        }
        
        if (model.getRowCount() > 0) {
            for (int i = model.getRowCount() - 1; i > -1; i--) {
                model.removeRow(i);
            }
        }
    }
        
    /*
    * Create Methods
    */
    public static void createMysqlTableRow(String id, String nome, String idade, String profissao) {
        DefaultTableModel model = (DefaultTableModel) mysqlTable.getModel();
        
        model.addRow(new Object[]{id, nome, idade, profissao});
    }
    
    public static void createPsqlTableRow(String id, String nome, String idade, String profissao) {
        DefaultTableModel model = (DefaultTableModel) psqlTable.getModel();
        
        model.addRow(new Object[]{id, nome, idade, profissao});
    }
    
    public static void createFileTableRow(String result) {
        DefaultTableModel model = (DefaultTableModel) fileTable.getModel();
        
        model.addRow(new Object[]{result});
    }
    
    public static void createGoogleTableRow(String link) {
        DefaultTableModel model = (DefaultTableModel) googleTable.getModel();
        
        model.addRow(new Object[]{link});
    }
    
    public static boolean isInteger(String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),10) < 0) return false;
        }
        return true;
    }
}
